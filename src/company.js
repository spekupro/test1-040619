const faker = require('faker');

/**
 * returns company by id
 *
 * @param {number} id
 * @returns {*}
 */
module.exports = function company(id) {
    if (id !== parseInt(id, 10)) {
        throw new Error('id needs to be integer');
    }
    faker.seed(id);
    return {
        companyId: id,
        name: faker.company.companyName(),
        address: {
            city: faker.address.city(),
            zipCode: faker.address.zipCode(),
            county: faker.address.county(),
            streetAddress: faker.address.streetAddress(),
        },
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        homepage: faker.internet.url(),
        ceo: faker.name.findName(),
    };
};
